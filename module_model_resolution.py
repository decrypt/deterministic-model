#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 14:07:15 2020

@author: Marie Haghebaert
READ ME :
- This modul is used to solve the deterministic limit of the Host-microbiota
    interaction model proposed by L. Darrigade in he's thesis.
- All cell densities and metabolite concentrations are save in the
    variable : y_sol in the following order :
        Densities : y_sol[0] : stem cells
                    y_sol[1] : DCS (constant)
                    y_sol[2] : progenitor cells
                    y_sol[3] : goblet cells
                    y_sol[4] : enterocyte
        Concentrations : y_sol[5] : O2
                         y_sol[6] : butyrate
    Therefore y_sol as y_0 are of dimension (7, Nz) where Nz is the number of
    discretisation point in the interval [0, Z_max]

- Cells are subject to cell fate. Users have acces to cell fate rates stocked 
in the variable : cell_fate_nb (7, Nz) in the following order :
        cell_fate_nb[0] : number of stem cells produced by division
        cell_fate_nb[0] : number of stem cells differenciated in progenitor cells
        cell_fate_nb[0] : number of progenitor cells produced by division
        cell_fate_nb[0] : number of progenitor cells differenciated in goblet cells
        cell_fate_nb[0] : number of progenitor cells differenciated in enterocytes
        cell_fate_nb[0] : number of goblet cells extruded
        cell_fate_nb[0] : number of enterocytes extruded

- In order to simplify the number of function's argument parameters are stored
    in dictionnaries :
        - dico_param : model's parameter
        - dico_param_num : numeric resolution's parameters
        - dico_space : space-dependent terms stored in np.ndarrays which only
            need to be calculated once and not at each resolution time

The following code is organised in different parts: 
    - Load modules and libraries 
    - resolution function (resolution, output_derivatives)
    - functions related to space-dependent terms (regulation_r, sigmoide, 
                                                 Get_f_tronq, Get_f_tronq_prime, 
                                                 Get_f_tronq_second, Get_phi,
                                                 Get_phi_prime, Get_phi_second)
    - functions related to the term G of the porous media equation type (
        Get_G, Get_G_prime, Get_G_second)
    - functions related to the application of kinetic diffusive explicit scheme
    proposed by Aregba-Driollet and all (2003). (density_discret_kinetic_scheme, 
                                                 Get_a_transport, 
                                                 Get_a_transport_derivative)
    - Thomas algorithme : a simplified form of Gaussian elimination that can be 
    used to solve tridiagonal systems of equations. this algo is used to update 
    concentrations of O2 and butyrate.
    - DCS shape is obtained using the function : dcs_shape
    - Functions related to results display (show_comparison_PDMP_PME, 
                                            display_densities, 
                                            display_concentrations, 
                                            show_cell_fate, 
                                            show_cell_fate_rate, 
                                            show_dcs_shape)

modified 20/02/20 by adding the functions density_discret_kinetic_scheme_Ceq1_conservative, a_transport_Ceq1_conservative and a_transport_derivative_Ceq1_conservative to sole the conservative form of the PME with C(z) = 1 for all z. 
"""

# Import Python packages
import numpy as np
from scipy.integrate import simps
from numba import jit
import matplotlib.pyplot as plt

# Import module
from module_kinetic_scheme import Get_A_B_conserv_and_dt


def resolution(
    y_0: np.ndarray,
    dico_param: dict,
    dico_param_num: dict,
    dico_space: dict,
    tol: float = 1e-14,
    display_nout: bool = False,
) -> (np.ndarray, int, np.ndarray, float):
    """
    Resolution function

    Parameters
    ----------
    y_0 : np.ndarray (7,dim(z_coord)): initial state for densities and
            concentrations
    dico_param : dict : dictionnary containing model's parameters
    dico_param_num : dict : dictionnary containing nemeric parameters used for
            resolution
    dico_space : dict : dictionnary with all variable concerning space which
                do not need to be recomputed every time
    tol : float (option) : precision tolerance for the calculation of the
            iteration error used as one of the stopping criteria
    display_nout : bool (option) user can choose to display graphe of densities
            and concentrations every nout iterations. nout is a parameter given
            in dico-param_num.

    Returns
    ---------
    y_sol : np.ndarray: final solution of the model
    n_iter : int : number of iteraction needed to get y_sol
    cell_fate_nb : np.ndarray : Number of cells counting in the cell fate at
            the final time
    time : float : final time
    """
    y_sol = np.copy(y_0)
    y_save = np.ones_like(y_0)

    time = 0
    n_iter = 0

    # Stop criterion parameters
    err_nout_old = 1000
    err_nout_new = 10
    err_nout = np.abs(err_nout_old - err_nout_new)
    y_nout = 10 * np.ones_like(y_0)

    # Resolution without display
    if not display_nout:
        while time < dico_param_num["Tmax"] and err_nout > tol:
            # Get refernce value of the relative error every nout iterations
            if n_iter % dico_param_num["nout"] == 0:
                err_nout_new = max(
                    np.max(np.abs(y_sol[i] - y_nout[i]))
                    / np.max(np.abs(y_nout[i]) + 10 * -8)
                    for i in [0, 2, 3, 4, 5, 6]
                )
                err_nout = np.abs(err_nout_old - err_nout_new)
                err_nout_old = err_nout_new
                y_nout = y_sol

            # Time step update
            dico_param_num["dt"] = min(
                dico_param_num["dt"], dico_param_num["Tmax"] - time
            )

            y_save = np.copy(y_sol)

            # Resolution
            y_sol, dico_param_num["dt"], cell_fate_nb = output_derivatives(
                y_save, dico_param, dico_param_num, dico_space
            )

            time += dico_param_num["dt"]
            n_iter += 1

    # Resolution with densities and concentrations display every nout iterations
    else:
        while time < dico_param_num["Tmax"] and err_nout > tol:
            # Get refernce value of the relative error every nout iterationsand plot graphs
            if n_iter % dico_param_num["nout"] == 0:
                # Save a reference error every nout iteration
                err_nout_new = max(
                    np.max(np.abs(y_sol[i] - y_nout[i])) / np.max(np.abs(y_nout[i]))
                    for i in [0, 2, 3, 4, 5, 6]
                )
                err_nout = np.abs(err_nout_old - err_nout_new)
                err_nout_old = err_nout_new
                y_nout = y_sol

                # plot
                display_densities(dico_space["z_coord"], y_sol, time)
                display_concentrations(dico_space["z_coord"], y_sol, time)

            # Time step update
            dico_param_num["dt"] = min(
                dico_param_num["dt"], dico_param_num["Tmax"] - time
            )

            y_save = np.copy(y_sol)
            # Resolution
            y_sol, dico_param_num["dt"], cell_fate_nb = output_derivatives(
                y_save, dico_param, dico_param_num, dico_space
            )

            time += dico_param_num["dt"]
            n_iter += 1

    return y_sol, n_iter, cell_fate_nb, time


def output_derivatives(
    y_save: np.ndarray, dico_param: dict, dico_param_num: dict, dico_space: dict,
) -> (np.ndarray, float, np.ndarray):
    """
    Retrun right hand side of discrtised EPD

    Parameters
    ----------
    y_save : np.ndarray (7,dim(z_coord)): Y_sol saved at time n
    dico_param : dict : dictionnary containing model's parameters
    dico_param_num : dict : dictionnary containing nemeric parameters used for
            resolution
    dico_space : dict : dictionnary with all variable concerning space which
                do not need to be recomputed every time

    Returns
    ---------
    y_sol : np.ndarray: solution at time n+1
    cfl : float : new cfl given by the kinetic scheme
    fate_cell : np.ndarray : Cell fate rates 
    """

    y_sol = np.copy(y_save)

    # total density in the crypt
    rho = y_sol[0] + y_sol[1] + y_sol[2] + y_sol[3] + y_sol[4]

    #  ***************** calculation of source terms for densities ************

    # Stem cells division rate
    rate_q_sc = (
        dico_param["q_div_sc"]
        * (1 - dico_space["R_sc"])
        * (
            1
            - regulation_r(
                dico_space["C"] * rho * dico_param["D_barre"],
                dico_param["K_cont_div_sc"],
                dico_param["l_cont_div_sc"],
            )
        )
        * (1 - regulation_r(y_sol[6], dico_param["K_but_sc"], dico_param["l_but"]))
    )

    # Stem cell differentiationin rate
    rate_q_sc_pc = dico_param["q_sc_pc"] * dico_space["R_sc"]

    # Progenitor cells division rate
    rate_q_pc = (
        dico_param["q_div_pc"]
        * (1 - dico_space["R_pc"])
        * (
            1
            - regulation_r(
                dico_space["C"] * rho * dico_param["D_barre"],
                dico_param["K_cont_div_pc"],
                dico_param["l_cont_div_sc"],
            )
        )
    )
    # Progenitor cells differentiation rate in goblet cells
    rate_q_pc_gc = (
        dico_param["q_pc_gc"]
        * dico_space["R_pc_gc_ent"]
        * regulation_r(y_sol[6], dico_param["K_but_pc_dif"], dico_param["l_but"])
    )

    # Progenitor cells differentiation rate in enterocyte cells
    rate_q_pc_ent = (
        dico_param["q_pc_ent"]
        * dico_space["R_pc_gc_ent"]
        * regulation_r(y_sol[6], dico_param["K_but_pc_dif"], dico_param["l_but"])
    )

    # Extrusion rate for goblet and enterocyte cells
    rate_q_ex_gc_ent = (
        dico_param["q_ex"]
        * dico_space["R_ext"]
        * regulation_r(
            dico_space["C"] * rho * dico_param["D_barre"],
            dico_param["K_cont_ext"],
            dico_param["l_cont_div_sc"],
        )
    )

    # Cell fate rates
    fate_cell = np.array(
        [
            rate_q_sc,
            rate_q_sc_pc,
            rate_q_pc,
            rate_q_pc_gc,
            rate_q_pc_ent,
            rate_q_ex_gc_ent,
        ]
    )
    # ******************** Kinetic scheme for densities **********************

    # Stem cells
    y_sol[0], cfl_sc = density_discret_kinetic_scheme_Ceq1_conservative(
        y_sol[0],  # Stem cells density at time n -1
        rho,  # total density
        rate_q_sc * y_sol[0],  # positiv source terms (explicit)
        rate_q_sc_pc,  # negativ source terms (implicit)
        dico_param["cond_sc_z0"],  # Dirichlet condition at the bottom
        dico_param["W_barre"],
        dico_param_num,
        dico_space,
    )

    # Progenitor cells
    y_sol[2], cfl_pc = density_discret_kinetic_scheme_Ceq1_conservative(
        y_sol[2],
        rho,
        rate_q_sc_pc * y_sol[0] + rate_q_pc * y_sol[2],
        rate_q_pc_gc + rate_q_pc_ent,
        dico_param["cond_pc_z0"],
        dico_param["W_barre"],
        dico_param_num,
        dico_space,
    )

    # Goblet cells
    y_sol[3], cfl_gc = density_discret_kinetic_scheme_Ceq1_conservative(
        y_sol[3],
        rho,
        rate_q_pc_gc * y_sol[2],
        rate_q_ex_gc_ent,
        dico_param["cond_gc_z0"],
        dico_param["W_barre"],
        dico_param_num,
        dico_space,
    )

    # Enterocyte cells
    y_sol[4], cfl_ent = density_discret_kinetic_scheme_Ceq1_conservative(
        y_sol[4],
        rho,
        rate_q_pc_ent * y_sol[2],
        rate_q_ex_gc_ent,
        dico_param["cond_ent_z0"],
        dico_param["W_barre"],
        dico_param_num,
        dico_space,
    )

    # ******************* Concentration discretization ************************

    # Dirichlet at the bottom for O2
    y_sol[5][0] = dico_param["c_o_bot"]
    # Dirichlet at the top for butyrate
    y_sol[6][-1] = dico_param["c_b_top"]

    # ************ calculation of source terms for concentrations ************
    # O2
    source_o = (
        (-4 * dico_param["gamma"] * (y_sol[5] ** 3 * y_sol[6]))
        / (y_sol[5] ** 4 * y_sol[6] + dico_param["K_b_o"])
        * (y_sol[3] + y_sol[4])
    )

    # Butyrate
    source_b = (
        -1
        * dico_param["gamma"]
        * (y_sol[5] ** 4)
        / (y_sol[5] ** 4 * y_sol[6] + dico_param["K_b_o"])
        * (y_sol[3] + y_sol[4])
    )

    # numeric constants
    cst_o = dico_param["sigma_o"] * dico_param_num["dt"] / dico_param_num["dz"] ** 2
    cst_b = dico_param["sigma_b"] * dico_param_num["dt"] / dico_param_num["dz"] ** 2

    # matrix formatting for concentration resolution with consideration of
    # boundary conditions
    id_vect = np.ones(dico_param_num["nz"])
    matrix_o = np.array(
        [
            -cst_o * id_vect,
            id_vect
            - dico_param_num["dt"] * source_o[1:]
            + cst_o * np.append(2 * id_vect[:-1], 1),
            -cst_o * id_vect,
        ]
    ).T
    matrix_b = np.array(
        [
            -cst_b * id_vect,
            id_vect
            - dico_param_num["dt"] * source_b[:-1]
            + cst_b * np.insert(2 * id_vect[:-1], 0, 1),
            -cst_b * id_vect,
        ]
    ).T

    # Concentration resolution using Thomas algorithme
    y_sol[5][1:] = thomas(
        matrix_o,
        y_sol[5][1:]
        + np.insert(
            np.zeros(dico_param_num["nz"] - 1), 0, cst_o * dico_param["c_o_bot"]
        ),
    )
    y_sol[6][:-1] = thomas(
        matrix_b,
        y_sol[6][:-1]
        + np.append(np.zeros(dico_param_num["nz"] - 1), cst_b * dico_param["c_b_top"]),
    )

    return y_sol, min(cfl_sc, cfl_pc, cfl_gc, cfl_ent), fate_cell


# ************************** Activation functions******************************


@jit(nopython=True)
def regulation_r(x_coord: np.ndarray, regul_k: float, regul_l: float) -> np.ndarray:
    """
    Activation function

    Parameters
    ----------
    x : np.ndarray
    regul_k : float
        Pivot value
    l : float
        DESCRIPTION.
    """
    return np.array(
        [
            1
            if x_coord[i] >= regul_k + regul_l
            else sigmoide(x_coord[i], regul_k, regul_l)
            if regul_k - regul_l < x_coord[i] < regul_k + regul_l
            else 0
            for i in range(x_coord.shape[0])
        ]
    )


@jit(nopython=True)
def sigmoide(z_coord: np.ndarray, regul_k: float, regul_l: float) -> np.ndarray:
    """
    Used with activation function return sigmoide curve 

    Parameters
    ----------
    z_coord : float
    regul_k : float
        Pivot value.
    l : float
        stiffness parameter.

    """
    a_cst = -1 / (4 * regul_l ** 3)
    b_cst = (3 * regul_k) / (4 * regul_l ** 3)
    c_cst = -(3 * regul_k ** 2 - 3 * regul_l ** 2) / (4 * regul_l ** 3)
    d_cst = (regul_k ** 3 + 2 * regul_l ** 3 - 3 * regul_k * regul_l ** 2) / (
        4 * regul_l ** 3
    )

    return a_cst * z_coord ** 3 + b_cst * z_coord ** 2 + c_cst * z_coord + d_cst


def Get_f_tronq(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    function that returns the shape of the truncated crypt at point z 

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

     """
    if z <= r0 - epsil:
        return r0 * np.sqrt((z + epsil) / r0 * (2 - (z + epsil) / r0))
    elif z >= Zmax - r0 + epsil:
        return r0 * (
            2 - np.sqrt((Zmax - z + epsil) / r0 * (2 - (Zmax - z + epsil) / r0))
        )
    return r0


def Get_f_tronq_prime(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    function that returns the derivative shape of the truncated crypt at point z 

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

     """

    if z < r0 - epsil:
        return (r0 - z - epsil) / np.sqrt(r0 ** 2 - (r0 - (z + epsil)) ** 2)
    elif z > Zmax - r0 + epsil:
        return (r0 - Zmax + z - epsil) / np.sqrt(
            r0 ** 2 - (r0 - (Zmax - (z - epsil))) ** 2
        )
    return 0.0


def Get_f_tronq_second(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    function that returns the second derivative shape of the truncated 
    crypt at point z 

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

     """
    if z < r0 - epsil:
        return -(r0 ** 2) / (np.sqrt(r0 ** 2 - (r0 - z - epsil) ** 2)) ** 3
    elif z > Zmax - r0 + epsil:
        return r0 ** 2 / (np.sqrt(r0 ** 2 - (r0 - (Zmax - (z - epsil))) ** 2)) ** 3
    return 0.0


def Get_phi(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    Returns the phi curvature effect such that the function is zero at 
    the crypt boundary

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

    """
    f = Get_f_tronq(z, Zmax, r0, epsil)
    f0 = Get_f_tronq(0, Zmax, r0, epsil)
    f_Zmax = Get_f_tronq(Zmax, Zmax, r0, epsil)
    if z < r0 - epsil:
        return (f / r0 - f0 / r0) / (1 - f0 / r0)
    elif z > Zmax - r0 + epsil:
        return (f_Zmax / r0 - f / r0) / (f_Zmax / r0 - 1)
    return 1


def Get_phi_prime(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    Returns the derivative of phi curvature effect such that the function 
    is zero at the crypt boundary

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

    """
    f0 = Get_f_tronq(0, Zmax, r0, epsil)
    f_Zmax = Get_f_tronq(Zmax, Zmax, r0, epsil)
    f_prime = Get_f_tronq_prime(z, Zmax, r0, epsil)
    if z < r0 - epsil:
        return f_prime / (r0 - f0)
    elif z > Zmax - r0 + epsil:
        return -f_prime / (f_Zmax - r0)
    return 0


def Get_phi_second(z: float, Zmax: float, r0: float, epsil: float) -> float:
    """
    Returns the seconde derivative of phi curvature effect such that the 
    function is zero at the crypt boundary

    Parameters
    ----------
    z : float
        space point 
    Z_max : float
        maximum height of the crypt
    r0 : float
        crypt radius.
    epsil : float
        Radius from which truncation is performed

    """
    f0 = Get_f_tronq(0, Zmax, r0, epsil)
    f_Zmax = Get_f_tronq(Zmax, Zmax, r0, epsil)
    f_second = Get_f_tronq_second(z, Zmax, r0, epsil)
    if z < r0 - epsil:
        return f_second / (r0 - f0)
    elif z > Zmax - r0 + epsil:
        return -f_second / (f_Zmax - r0)
    return 0


def Get_G(C: np.ndarray, phi: np.ndarray, W: float) -> np.ndarray:
    """
    Gives the variable G present in the reformulation of the porous media 
    equation used with the diffusive kinetic scheme
    
    Parameters
    ----------
    C : np.ndarray
        Curvature correction of the crypt on the density.
    phi : np.ndarray
        Curvature correction of the crypt on mecanic interaction force.
    W : float
        interaction force.

    """
    return W * phi * C / 2


def Get_G_prime(
    C: np.ndarray, C_prime: np.ndarray, phi: np.ndarray, phi_prime: np.ndarray, W: float
) -> np.ndarray:
    """
    Gives the derivative of variable G present in the reformulation of the 
    porous media equation used with the diffusive kinetic scheme

    Parameters
    ----------
    C : np.ndarray
        Curvature correction of the crypt on the density.
    C_prime : np.ndarray
        Derivative of the curvature correction of the crypt on the density.
    phi : np.ndarray
        Curvature correction of the crypt on mecanic interaction force.
    phi_prime : np.ndarray
        Derivative of the curvature correction of the crypt on mecanic interaction force.
    W : float
        interaction force..

    """
    return W * (phi_prime * C + phi * C_prime) / 2


def Get_G_second(
    C: np.ndarray,
    C_prime: np.ndarray,
    C_second: np.ndarray,
    phi: np.ndarray,
    phi_prime: np.ndarray,
    phi_second: np.ndarray,
    W: float,
) -> np.ndarray:
    """
    Gives the derivative of variable G present in the reformulation of 
    the porous media equation used with the diffusive kinetic scheme

    Parameters
    ----------
    C : np.ndarray
        Curvature correction of the crypt on the density.
    C_prime : np.ndarray
        Derivative of the curvature correction of the crypt on the density.
    C_second : np.ndarray
        Second derivative of the curvature correction of the crypt on 
        the density.
    phi : np.ndarray
        Curvature correction of the crypt on mecanic interaction force.
    phi_prime : np.ndarray
        Derivative of the curvature correction of the crypt on mecanic 
        interaction force.
    phi_second : np.ndarray
        Second derivative of the curvature correction of the crypt on 
        mecanic interaction force.
    W : float
        interaction force.

    """
    return W * (phi_second * C + 2 * phi_prime * C_prime + phi * C_second) / 2


# ************** Numeric Resolution functions for densities *******************

def density_discret_kinetic_scheme_Ceq1_conservative(
    rho_l: np.ndarray,
    rho: np.ndarray,
    source_pos: np.ndarray,
    source_neg: np.ndarray,
    z0_cond: float,
    w_param: float,
    dico_param_num: dict,
    dico_space: dict,
):
    """
    Return rho at the time n+1 using kinetic diffusiv scheme from reference
    Aregba-Driollet and al.

    Parameters
    ----------
    rho_l : np.ndarray
        density of type l at time n
    rho : np.ndarray
        Total density at time n
    source_pos : np.ndarray
        positive source terms
    source_neg : np.ndarray
        negative source terms
    z0_cond : float
        boundary Dirichlet condition at the top of the crypt
    w_param : float
        Parameter concerning physic repulsion between cells
    dico_param_num : dict
        dictionnary containing all numeric parameters used for resolution
    dico_space : dict
        dictionnary containing all spce modelisation choice (activiation in
        space, shape of the crypte ) which do not need to be recalculated
        every time

    Returns
    -------
    rho_l : np.ndarray
        density of type l at time n+1
    dt_new : float
        new time stap

    """
    # function A corresponds to the transport term of the reconditioned equation
    a_transport = get_a_transport_Ceq1_conservative(
        rho_l,
        rho,
        w_param,
        dico_param_num["dz"],
        dico_space["phi"],
        dico_space["phi_prime"],
    )

    # derivative according to rho_l of the function a_transport
    a_transport_derivative = get_a_transport_derivative_Ceq1_conservative(
        rho_l,
        rho,
        w_param,
        dico_param_num["dz"],
        dico_space["phi"],
        dico_space["phi_prime"],
    )

    # function b_diffusion corresponds to the diffusive term of the reconditioned equation
    b_diffusion = dico_space["G"] * rho_l ** 2

    # derivative according to rho_l of the function b_diffusion
    b_diffusion_derivative = 2 * dico_space["G"] * rho_l

    # Calculation of A ronde and B ronde using the kinetic diffusive scheme
    b_transport_conserv, b_diffusion_conserv, dt_new = Get_A_B_conserv_and_dt(
        rho_l,
        a_transport,
        a_transport_derivative,
        b_diffusion,
        b_diffusion_derivative,
        dico_param_num["alpha"],
        dico_param_num["dt"],
        dico_param_num["dz"],
        dico_param_num["m"],
    )

    dt_dz = dico_param_num["dt"] / dico_param_num["dz"]
    dt_dz2 = dico_param_num["dt"] / (dico_param_num["dz"] ** 2)

    # Density update negativ source terms are implicit
    # Density update negativ source terms are implicit
    rho_l[1:-1] = (
        rho_l[1:-1]
        - dt_dz * (b_transport_conserv[1:] - b_transport_conserv[:-1])
        + dt_dz2
        * (
            b_diffusion_conserv[:-2]
            - 2 * b_diffusion_conserv[1:-1]
            + b_diffusion_conserv[2:]
        )
        + dico_param_num["dt"] * source_pos[1:-1]
    ) / (1 + dico_param_num["dt"] * source_neg[1:-1])

    # Dirichlet condition at the bottom of the crypt
    rho_l[0] = z0_cond
    # Neumann condition at the top of the crypt
    rho_l[-1] = rho_l[-2]

    return rho_l, dt_new
    
    


    @staticmethod
    @jit(nopython=True)
    def get_a_transport_Ceq1_conservative(
        rho_l: np.ndarray,
        rho: np.ndarray,
        w_param: float,
        step_dz: float,
        phi: np.ndarray,
        phi_derivative: np.ndarray,
    ) -> np.ndarray:
        """
        Notation Aregba-Driollet and al. function A corresponding to
        the transport term according to the reconditioned equation
        taking into account that C(z) = 1 for all z and using the conservative 
        form of the equation.
    
        Parameters
        ----------
        rho_l : numpy array
            cell type l density 
        rho : numpy array
            total density
        w_param : float
            Interaction force value. 
        step_dz : float
            space step 
        phi : numpy array
            Curvature effects on mecanics interactions between cells  
        phi_derivative : numpy array
            Derivative of curvature effects on mecanics interactions between cells  
        g_derivative : numpy array 
            notation term comming from equation actually g(z) = w_param * c_curvature * phi / 2  
    
        Returns
        -------
        numpy array
            a_transport.
    
        """
        a_transport = np.zeros(rho.shape[0])

        hat_rho_l =  rho - rho_l

        a_transport[:-1] = (
            - w_param * phi[:-1] * rho_l[:-1] * np.diff(hat_rho_l) / step_dz
            + (w_param * phi_derivative[:-1] /2)* rho_l[:-1] ** 2
        )

        a_transport[-1] = (
            - w_param * phi[-1] *  rho_l[-1] * (hat_rho_l[-1] - hat_rho_l[-2]) / step_dz
            + ( w_param * phi_derivative[-1] / 2) * rho_l[-1] ** 2
        )
        return a_transport



    @staticmethod
    @jit(nopython=True)
    def get_a_transport_derivative_Ceq1_conservative(
        rho_l: np.ndarray,
        rho: np.ndarray,
        w_param: float,
        step_dz: float,
        phi: np.ndarray,
        phi_derivative: np.ndarray,
    ) -> np.ndarray:
        """
        Notation Aregba-Driollet and al. derivative according to rho_l of
        the function a_transport
    
        Parameters
        ----------
        rho_l : numpy array
            cell type l density 
        rho : numpy array
            total density
        w_param : float
            Interaction force value. 
        step_dz : float
            space step 
       phi : numpy array
            Curvature effects on mecanics interactions between cells  
        phi_derivative : numpy array
            Derivative of curvature effects on mecanics interactions between cells  
        g_derivative : numpy array 
            notation term comming from equation actually g(z) = w_param * c_curvature * phi / 2  
    
        Returns
        -------
        numpy array
            a_transport_derivative.
    
        """

        a_transport_derivative = np.zeros(rho.shape[0])

        hat_rho_l = rho - rho_l

        a_transport_derivative[:-1] = (
            - w_param * phi[:-1] * (np.diff(hat_rho_l) / step_dz) 
            + (w_param * phi_derivative[:-1])* rho_l[:-1]
        )

        a_transport_derivative[-1] = (
            - w_param * phi[-1] * ((hat_rho_l[-1] - hat_rho_l[-2]) / step_dz) 
            + ( w_param * phi_derivative[-1] ) * rho_l[-1]
        )
        return  a_transport_derivative
        
        
def density_discret_kinetic_scheme(
    rho_l: np.ndarray,
    rho: np.ndarray,
    source_pos: np.ndarray,
    source_neg: np.ndarray,
    z0_cond: float,
    w_param: float,
    dico_param_num: dict,
    dico_space: dict,
):
    """
    Return rho at the time n+1 using kinetic diffusiv scheme from reference
    Aregba-Driollet and al.

    Parameters
    ----------
    rho_l : np.ndarray
        density of type l at time n
    rho : np.ndarray
        Total density at time n
    source_pos : np.ndarray
        positive source terms
    source_neg : np.ndarray
        negative source terms
    z0_cond : float
        boundary Dirichlet condition at the top of the crypt
    w_param : float
        Parameter concerning physic repulsion between cells
    dico_param_num : dict
        dictionnary containing all numeric parameters used for resolution
    dico_space : dict
        dictionnary containing all spce modelisation choice (activiation in
        space, shape of the crypte ) which do not need to be recalculated
        every time

    Returns
    -------
    rho_l : np.ndarray
        density of type l at time n+1
    dt_new : float
        new time stap

    """
    # function A corresponds to the transport term of the reconditioned equation
    a_transport = get_a_transport(
        rho_l,
        rho,
        w_param,
        dico_param_num["dz"],
        dico_space["phi"],
        dico_space["C"],
        dico_space["C_prime"],
        dico_space["G_prime"],
    )

    # derivative according to rho_l of the function a_transport
    a_transport_derivative = get_a_transport_derivative(
        rho_l,
        rho,
        w_param,
        dico_param_num["dz"],
        dico_space["phi"],
        dico_space["C"],
        dico_space["C_prime"],
        dico_space["G_prime"],
    )

    # function b_diffusion corresponds to the diffusive term of the reconditioned equation
    b_diffusion = dico_space["G"] * rho_l ** 2

    # derivative according to rho_l of the function b_diffusion
    b_diffusion_derivative = 2 * dico_space["G"] * rho_l

    # Calculation of A ronde and B ronde using the kinetic diffusive scheme
    b_transport_conserv, b_diffusion_conserv, dt_new = Get_A_B_conserv_and_dt(
        rho_l,
        a_transport,
        a_transport_derivative,
        b_diffusion,
        b_diffusion_derivative,
        dico_param_num["alpha"],
        dico_param_num["dt"],
        dico_param_num["dz"],
        dico_param_num["m"],
    )

    dt_dz = dico_param_num["dt"] / dico_param_num["dz"]
    dt_dz2 = dico_param_num["dt"] / (dico_param_num["dz"] ** 2)

    # Density update negativ source terms are implicit
    rho_l[1:-1] = (
        rho_l[1:-1]
        - dt_dz * (b_transport_conserv[1:] - b_transport_conserv[:-1])
        + dt_dz2
        * (
            b_diffusion_conserv[:-2]
            - 2 * b_diffusion_conserv[1:-1]
            + b_diffusion_conserv[2:]
        )
        + dico_param_num["dt"] * source_pos[1:-1]
        - (dico_param_num["dt"] * dico_space["G_second"][1:-1] * rho_l[1:-1] ** 2)
    ) / (1 + dico_param_num["dt"] * source_neg[1:-1])

    # Dirichlet condition at the bottom of the crypt
    rho_l[0] = z0_cond
    # Neumann condition at the top of the crypt
    rho_l[-1] = rho_l[-2]

    return rho_l, dt_new
     
@jit(nopython=True)
def get_a_transport(
    rho_l: np.ndarray,
    rho: np.ndarray,
    w_param: float,
    step_dz: float,
    phi: np.ndarray,
    curvature_c: np.ndarray,
    curvature_c_derivative: np.ndarray,
    g_derivative: np.ndarray,
) -> np.ndarray:
    """
    Notation Aregba-Driollet and al. function A corresponding to
    the transport term according to the reconditioned equation

    Parameters
    ----------
    rho_l : numpy array
        cell type l density 
    rho : numpy array
        total density
    w_param : float
        Interaction force value. 
    step_dz : float
        space step 
    phi : numpy array
        Curvature effcts on mecanics interactions between cells  
    curvature_c : numpy array
        Curvature effect on the density 
    curvature_c_derivative : numpy array
        Derivative of the curvature effect on the density 
    g_derivative : numpy array 
        notation term comming from equation actually g(z) = w_param * c_curvature * phi / 2  

    Returns
    -------
    numpy array
        a_transport.

    """
    a_transport = np.zeros(rho.shape[0])

    hat_rho_l = curvature_c * (rho - rho_l)

    a_transport[:-1] = (
        phi[:-1]
        * (
            curvature_c_derivative[:-1] * hat_rho_l[:-1]
            + curvature_c[:-1] * np.diff(hat_rho_l) / step_dz
        )
        * rho_l[:-1]
        + (phi[:-1] * curvature_c_derivative[:-1] + g_derivative[:-1] / w_param)
        * rho_l[:-1] ** 2
    )

    a_transport[-1] = (
        phi[-1]
        * (
            curvature_c_derivative[-1] * hat_rho_l[-1]
            + curvature_c[-1] * (hat_rho_l[-1] - hat_rho_l[-2]) / step_dz
        )
        * rho_l[-1]
        + (phi[-1] * curvature_c_derivative[-1] + g_derivative[-1] / w_param)
        * rho_l[-1] ** 2
    )
    return -w_param * a_transport


@jit(nopython=True)
def get_a_transport_derivative(
    rho_l: np.ndarray,
    rho: np.ndarray,
    w_param: float,
    step_dz: float,
    phi: np.ndarray,
    curvature_c: np.ndarray,
    curvature_c_derivative: np.ndarray,
    g_derivative: np.ndarray,
) -> np.ndarray:
    """
    Notation Aregba-Driollet and al. derivative according to rho_l of
    the function a_transport

    Parameters
    ----------
    rho_l : numpy array
        cell type l density 
    rho : numpy array
        total density
    w_param : float
        Interaction force value. 
    step_dz : float
        space step 
    phi : numpy array
        Curvature effcts on mecanics interactions between cells  
    curvature_c : numpy array
        Curvature effect on the density 
    curvature_c_derivative : numpy array
        Derivative of the curvature effect on the density 
    g_derivative : numpy array 
        notation term comming from equation actually g(z) = w_param * c_curvature * phi / 2  

    Returns
    -------
    numpy array
        a_transport_derivative.

    """

    a_transport_derivative = np.zeros(rho.shape[0])

    hat_rho_l = curvature_c * (rho - rho_l)

    a_transport_derivative[:-1] = (
        phi[:-1]
        * (
            curvature_c_derivative[:-1] * hat_rho_l[:-1]
            + curvature_c[:-1] * np.diff(hat_rho_l) / step_dz
        )
        + 2
        * (phi[:-1] * curvature_c_derivative[:-1] + g_derivative[:-1] / w_param)
        * rho_l[:-1]
    )

    a_transport_derivative[-1] = (
        phi[-1]
        * (
            curvature_c_derivative[-1] * hat_rho_l[-1]
            + curvature_c[-1] * (hat_rho_l[-1] - hat_rho_l[-2]) / step_dz
        )
        + 2
        * (phi[-1] * curvature_c_derivative[-1] + g_derivative[-1] / w_param)
        * rho_l[-1]
    )
    return -w_param * a_transport_derivative


@jit(nopython=True)
def thomas(matrix_h: np.ndarray, vect_c: np.ndarray) -> np.ndarray:
    """
   Solve  system Hx = c where x is the unkown and matrix_h is the equivalent of
   a tridiagonal matrix stor in an array (dim,3)

    Parameters
    ----------
    matrix_h : numpy ndarray de dim n*3
        Matrix that stor subdiagonal,diagonal and supdiagonal
    vect_c : numpy ndarray dim n
        Source term of the  system
    """
    dim = np.shape(vect_c)[0]
    x_unknown = np.zeros(dim)
    copy_h = np.copy(matrix_h)
    copy_c = np.copy(vect_c)
    for i in range(1, dim):
        copy_h[i, 1] = (
            copy_h[i, 1] - (copy_h[i, 0] / copy_h[i - 1, 1]) * copy_h[i - 1, 2]
        )
        copy_c[i] = copy_c[i] - (copy_h[i, 0] / copy_h[i - 1, 1]) * copy_c[i - 1]

    x_unknown[-1] = copy_c[-1] / copy_h[-1, 1]

    for i in range(dim - 2, -1, -1):
        x_unknown[i] = (copy_c[i] - copy_h[i, 2] * x_unknown[i + 1]) / copy_h[i, 1]

    return x_unknown


def dcs_shape(
    z_coord: float, z_a: float, slope_a: float, z_b: float, slope_b: float
) -> float:
    """
    Function used to define DCS cells density shape.

    Parameters
    ----------
    z_coord : float
        point space
    z_a : float
        minimal point space
    slope_a : float
       slope of the line in the minimal side
    z_b : float
        maximal point space.
    slope_b : float
        slope of the line in the maximal side.
    """
    if slope_b > 0:
        raise ValueError('"slope_b" must be negative.')
    if slope_a < 0:
        raise ValueError('"slope_a" must be positive.')
    if z_a > z_b:
        raise ValueError('"z_b" must be higher than "z_a".')

    # minimal line
    if z_a - 1 / slope_a < z_coord < z_a:
        return slope_a * (z_coord - z_a) + 1
    # maximal line
    elif z_b < z_coord < z_b - 1 / slope_b:
        return slope_b * (z_coord - z_b) + 1
    # between minimal and maximal line
    elif z_a <= z_coord <= z_b:
        return 1
    # elsewhere
    return 0


# *****************************************************************************
# *************************** Display *****************************************
# *****************************************************************************


def show_comparison_PDMP_PME_density(
    load_file_density: str,
    load_file_stand_dev: str,
    z_coord: np.ndarray,
    y: np.ndarray,
    colors: np.ndarray = np.array([]),
    file_name: str = "graphe_compare_PDMP_PME_for_densities",
    save: bool = 0,
    title: bool = False,
) -> bool:
    """
    Display function used to compare results from individual-based model
    and deterministic model.

    Parameters
    ----------
    load_file_densit : str 
        Name of the file with densities from PDMP model
    load_file_stand_dev : str 
        Name of the file with standart deviatopn for densities from PDMP model
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    time : float
    y : np.ndarray
        densitiy obtained from deterministic model.
    colors : np.ndarray optionnal 
            plot colors  
    file_name : str
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title:TYPE, optional = False
        Display graphe titles 
    """

    if colors.shape[0] < 1:
        import matplotlib.colors as mcolors

        clrs = list(mcolors.TABLEAU_COLORS.values())
    else:
        clrs = colors

    title_set = [
        "Stem",
        "DCS",
        "Progenitor",
        "Goblet",
        "Enterocyte",
    ]

    import pickle

    dict_dens = pickle.load(open(load_file_density, "rb"))
    dict_std = pickle.load(open(load_file_stand_dev, "rb"))
    dict_cell_title = ["sc", "dcs", "pc", "gc", "ent"]
    bin_z = 200 / dict_dens["sc"].shape[0]

    z_bar = np.arange(0, 200, bin_z)
    for i in range(len(dict_cell_title)):
        plt.figure(figsize=(7, 2))
        plt.grid("major", color="gray", linestyle="dashed", linewidth=0.3)
        plt.plot(z_coord, y[i], color="red", lw=3, label="PME")

        plt.bar(
            z_bar,
            dict_dens[dict_cell_title[i]],
            bin_z,
            align="edge",
            color=clrs[i],
            edgecolor="black",
            yerr=dict_std[dict_cell_title[i]],
            alpha=0.4,
            ecolor="black",
            label="PDMP",
        )
        plt.xlim(0, 200)
        plt.ylim(0, 5)
        plt.xticks(np.arange(0, 210, 20))
        plt.ylabel(title_set[i])
        plt.xlabel("Position ($\\mu m$)")
        plt.legend()
        plt.title(title_set[i])
        if save:
            import tikzplotlib

            # tikzplotlib.clean_figure()
            tikzplotlib.save(
                "TEX/" + file_name + "_" + dict_cell_title[i] + ".tex",
                axis_height="5cm",
                axis_width="11cm",
            )
        plt.show()
    return 1


def show_comparison_PDMP_PME_cell_fate_rates(
    load_file_rates: str,
    load_file_stand_dev: str,
    z_coord: np.ndarray,
    y_sol: np.ndarray,
    rates_PME: np.ndarray,
    colors: np.ndarray = np.array([]),
    file_name: str = "graphe_compare_PDMP_PME_for_densities",
    save: bool = 0,
    title: bool = False,
) -> bool:
    """
    Display function used to compare results from individual-based model
    and deterministic model.

    Parameters
    ----------
    load_file_rates : str 
        Name of the file with cell fate rates from PDMP model
    load_file_stand_dev : str 
        Name of the file with standart deviatopn for cell fate rates from PDMP model
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    time : float
    y : np.ndarray
        densitiy obtained from deterministic model.
    rates_PME : np.ndarray
        cell fate rates obtained from deterministic model.
    colors : np.ndarray optionnal 
            plot colors  
    file_name : str
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title:TYPE, optional = False
        Display graphe titles 
    """

    if colors.shape[0] < 1:
        import matplotlib.colors as mcolors

        clrs = list(mcolors.TABLEAU_COLORS.values())
    else:
        clrs = colors

    if not title:
        title = [""] * 4
    else:
        title = [
            "Stem",
            "Progenitor",
            "Goblet",
            "Enterocyte",
        ]

    import pickle

    dict_rates = pickle.load(open(load_file_rates, "rb"))
    dict_std = pickle.load(open(load_file_stand_dev, "rb"))

    cell_rates_PME = [
        rates_PME[0] * y_sol[0],
        rates_PME[1] * y_sol[0],
        rates_PME[2] * y_sol[2],
        rates_PME[3] * y_sol[2],
        rates_PME[4] * y_sol[2],
        rates_PME[5] * y_sol[3],
        rates_PME[5] * y_sol[4],
    ]

    cell_rate_PDMP = [
        dict_rates["sc"]["sc"],
        dict_rates["sc"]["pc"],
        dict_rates["pc"]["pc"],
        dict_rates["pc"]["gc"],
        dict_rates["pc"]["ent"],
        dict_rates["gc"]["ex"],
        dict_rates["ent"]["ex"],
    ]

    cell_rate_std_PDMP = [
        dict_std["sc"]["sc"],
        dict_std["sc"]["pc"],
        dict_std["pc"]["pc"],
        dict_std["pc"]["gc"],
        dict_std["pc"]["ent"],
        dict_std["gc"]["ex"],
        dict_std["ent"]["ex"],
    ]

    dict_cell_title = ["sc", "dcs", "pc", "gc", "ent"]
    bin_z = 200 / dict_rates["sc"]["sc"].shape[0]

    z_bar = np.arange(0, 200, bin_z)

    alp = 0.4

    # TO DO : optimize
    plt.figure(figsize=(7, 2))
    plt.grid("major", color="gray", linestyle="dashed", linewidth=0.3)
    plt.plot(z_coord, cell_rates_PME[0], color=clrs[0], lw=3, label="PME : div sc")
    plt.bar(
        z_bar,
        cell_rate_PDMP[0],
        bin_z,
        align="edge",
        color=clrs[0],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[0],
        alpha=alp,
        ecolor="black",
        label="PDMP : div sc ",
    )
    plt.plot(
        z_coord, cell_rates_PME[1], color=clrs[1], lw=3, label="PME : diff sc / pc "
    )
    plt.bar(
        z_bar,
        cell_rate_PDMP[1],
        bin_z,
        align="edge",
        color=clrs[1],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[1],
        alpha=alp,
        ecolor="black",
        label="PDMP : div sc / pc",
    )
    plt.xlim(0, 200)
    plt.xlim(0, 200)
    plt.ylim(0)
    plt.xticks(np.arange(0, 210, 20))
    plt.ylabel(" Fate events ( $Cell  \\times h^{-1}$)")
    plt.xlabel("Position ($\\mu m$)")
    plt.legend(loc="upper right", fontsize=9)
    plt.title(title[0])
    if save:
        import tikzplotlib

        # tikzplotlib.clean_figure()
        tikzplotlib.save(
            "TEX/" + file_name + "_" + dict_cell_title[0] + ".tex",
            axis_height="5cm",
            axis_width="11cm",
        )
    plt.show()

    # TO DO : optimize
    plt.figure(figsize=(7, 2))
    plt.grid("major", color="gray", linestyle="dashed", linewidth=0.3)
    plt.plot(z_coord, cell_rates_PME[2], color=clrs[2], lw=3, label="PME : div pc")
    plt.bar(
        z_bar,
        cell_rate_PDMP[2],
        bin_z,
        align="edge",
        color=clrs[2],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[2],
        alpha=alp,
        ecolor="black",
        label="PDMP : div pc",
    )
    plt.plot(
        z_coord, cell_rates_PME[4], color=clrs[4], lw=3, label="PME : diff pc / gc"
    )
    plt.bar(
        z_bar,
        cell_rate_PDMP[4],
        bin_z,
        align="edge",
        color=clrs[4],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[4],
        alpha=alp,
        ecolor="black",
        label="PDMP : diff pc / gc",
    )
    plt.plot(
        z_coord, cell_rates_PME[3], color=clrs[3], lw=3, label="PME : diff pc / ent"
    )
    plt.bar(
        z_bar,
        cell_rate_PDMP[3],
        bin_z,
        align="edge",
        color=clrs[3],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[3],
        alpha=alp,
        ecolor="black",
        label="PDMP : diff pc / ent",
    )

    plt.xlim(0, 200)
    plt.xlim(0, 200)
    plt.ylim(0, 0.5)
    plt.xticks(np.arange(0, 210, 20))
    plt.ylabel(" Fate events ( $Cell  \\times h^{-1}$)")
    plt.xlabel("Position ($\\mu m$)")
    plt.legend(loc="upper right", fontsize=9)
    plt.title(title[1])
    if save:
        import tikzplotlib

        # tikzplotlib.clean_figure()
        tikzplotlib.save(
            "TEX/" + file_name + "_" + dict_cell_title[2] + ".tex",
            axis_height="5cm",
            axis_width="11cm",
        )
    plt.show()

    # TO DO : optimize
    plt.figure(figsize=(7, 2))
    plt.grid("major", color="gray", linestyle="dashed", linewidth=0.3)
    plt.plot(z_coord, cell_rates_PME[5], color=clrs[5], lw=3, label="PME : ext gc")
    plt.bar(
        z_bar,
        cell_rate_PDMP[5],
        bin_z,
        align="edge",
        color=clrs[5],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[5],
        alpha=alp,
        ecolor="black",
        label="PDMP : ext gc",
    )
    plt.xlim(0, 200)
    plt.xlim(0, 200)
    plt.ylim(0)
    plt.xticks(np.arange(0, 210, 20))
    plt.ylabel(" Fate events ( $Cell  \\times h^{-1}$)")
    plt.xlabel("Position ($\\mu m$)")
    plt.legend(loc="upper right", fontsize=9)
    plt.title(title[2])
    if save:
        import tikzplotlib

        # tikzplotlib.clean_figure()
        tikzplotlib.save(
            "TEX/" + file_name + "_" + dict_cell_title[3] + ".tex",
            axis_height="5cm",
            axis_width="11cm",
        )
    plt.show()

    # TO DO : optimize
    plt.figure(figsize=(7, 2))
    plt.grid("major", color="gray", linestyle="dashed", linewidth=0.3)
    plt.plot(z_coord, cell_rates_PME[6], color=clrs[6], lw=3, label="PME : ext ent")
    plt.bar(
        z_bar,
        cell_rate_PDMP[6],
        bin_z,
        align="edge",
        color=clrs[6],
        edgecolor="black",
        yerr=cell_rate_std_PDMP[6],
        alpha=alp,
        ecolor="black",
        label="PDMP : ext ent",
    )
    plt.xlim(0, 200)
    plt.xlim(0, 200)
    plt.ylim(0)
    plt.xticks(np.arange(0, 210, 20))
    plt.ylabel(" Fate events ( $Cell  \\times h^{-1}$)")
    plt.xlabel("Position ($\\mu m$)")
    plt.legend(loc="upper right", fontsize=9)
    plt.title(title[3])
    if save:
        import tikzplotlib

        # tikzplotlib.clean_figure()
        tikzplotlib.save(
            "TEX/" + file_name + "_" + dict_cell_title[4] + ".tex",
            axis_height="5cm",
            axis_width="11cm",
        )
    plt.show()

    return 1


def display_densities(
    z_coord: np.ndarray,
    y_sol: np.ndarray,
    time: float,
    colors: np.ndarray = np.array([]),
    file_name: str = "graphe_densities_along_crypt",
    save: bool = 0,
    title: str = "",
) -> bool:
    """
    Display all cell densities along the crypt at a particular time

    Parameters
    ----------
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    y_sol : np.ndarray
        Variable storing densities
    colors : np.ndarray optionnal 
            plot colors  
    file_name : str, optional
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title : str : optionnal 
        Plot title 
    """
    rho = y_sol[0] + y_sol[1] + y_sol[2] + y_sol[3] + y_sol[4]
    lw2 = 2
    label = ["tot : %d", "sc : %d", "dcs : %d", "pc : %d", "gc : %d", "ent : %d"]

    if colors.shape[0] < 1:
        import matplotlib.colors as mcolors

        clrs = list(mcolors.TABLEAU_COLORS.values())
    else:
        clrs = colors

    plt.plot(
        z_coord,
        rho,
        label=label[0] % (np.round(simps(rho, z_coord))),
        lw=lw2,
        color=clrs[0],
    )
    for i in range(1, len(label)):
        plt.plot(
            z_coord,
            y_sol[i - 1],
            label=label[i] % (np.round(simps(y_sol[i - 1], z_coord))),
            lw=lw2,
            color=clrs[i],
        )

    plt.legend(loc="upper right")
    plt.xlabel("$ \mu m$")
    plt.ylabel("Density")
    plt.xlim(0, 200)
    plt.ylim(0, 5)
    plt.title(title)
    plt.grid("major", color="gray", linestyle="dashed", linewidth=1)
    if save:
        import tikzplotlib

        tikzplotlib.clean_figure()
        tikzplotlib.save("TEX/" + file_name + ".tex")
    plt.show()
    plt.clf()
    return 1


def display_concentrations(
    z_coord: np.ndarray,
    y_sol: np.ndarray,
    time: float,
    colors: np.ndarray = np.array([]),
    file_name: str = "graphe_concentrations_along_crypt",
    save: bool = 0,
    title: str = "",
) -> bool:
    """
    Display all metabolite concentrations along the crypt at a particular time

    Parameters
    ----------
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    y_sol : np.ndarray
        Variable storing concentrations
    time : float
    colors : np.ndarray optionnal 
            plot colors  
    file_name : str, optional
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title : str : optionnal 
        Plot title 
    """
    lw2 = 2
    plt.plot(z_coord, y_sol[5], label="$O^2$", lw=lw2)
    plt.plot(z_coord, y_sol[6], label="$But$", lw=lw2)

    plt.legend(loc="upper right")
    # plt.title(" Concentrations at time : " + str(np.round(time)) + "h")
    plt.xlabel("$\mu m$")
    plt.ylabel("Concentrations")
    plt.xlim(0, 200)
    plt.ylim(0, 12)
    plt.title(title)
    plt.grid("major", color="gray", linestyle="dashed", linewidth=1)
    if save:
        import tikzplotlib

        tikzplotlib.clean_figure()
        tikzplotlib.save("TEX/" + file_name + ".tex")
    plt.show()
    plt.clf()
    return 1


def show_cell_fate(
    fate_cell: np.ndarray,
    z_coord: np.ndarray,
    y_sol: np.ndarray,
    label_cell_fate: list,
    colors: np.ndarray = np.array([]),
    file_name: str = "TEX/cell_fate.tex",
    save: bool = 0,
    title: str = "",
) -> bool:
    """
    Display densities*rate of cell fate (cell.h^-1) in this order : 
        [div_sc * rho_sc, 
        diff_sc_pc * rho_sc, 
        div_pc * rho_pc, 
        diff_pc_gc * rho_pc, 
        diff_pc_ent * rho_pc,
        ext_gc * rho_gc, 
        ext_ent * rho_ent]

    Parameters
    ----------
    fate_cell : np.ndarray 
        Cell fate rates (division, extrusion..)
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    y_sol : np.ndarray
        Variable storing concentrations
    label_cell_fate: list
        list of string used for legend
    colors : np.ndarray optionnal 
            plot colors  
    file_name : str, optional
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title : str : optionnal 
        Plot title 
    
    """

    if colors.shape[0] < 1:
        import matplotlib.colors as mcolors

        clrs = list(mcolors.TABLEAU_COLORS.values())
    else:
        clrs = colors
    lwi = 3
    cell_rates_dens = [
        fate_cell[0] * y_sol[0],
        fate_cell[1] * y_sol[0],
        fate_cell[2] * y_sol[2],
        fate_cell[3] * y_sol[2],
        fate_cell[4] * y_sol[2],
        fate_cell[5] * y_sol[3],
        fate_cell[5] * y_sol[4],
    ]
    for i in range(fate_cell.shape[0]):

        plt.plot(
            z_coord,
            cell_rates_dens[i],
            label=label_cell_fate[i],
            lw=lwi,
            color=clrs[i],
        )

    plt.xlabel("$\mu m$")
    plt.legend(loc="upper right")
    plt.ylabel("$cell.h^{-1}$")
    plt.legend()
    plt.xlim(0, 200)
    plt.ylim(0)
    plt.legend(loc="upper right")
    plt.title(title)
    plt.grid("major", color="gray", linestyle="dashed", linewidth=1)
    if save:
        import tikzplotlib

        tikzplotlib.clean_figure()
        tikzplotlib.save("TEX/" + file_name + ".tex")
    plt.show()
    plt.clf()
    return 1


def show_cell_fate_rate(
    fate_cell: np.ndarray,
    z_coord: np.ndarray,
    label_cell_fate: list,
    colors: np.ndarray = np.array([]),
    file_name: str = "cell_fate.tex",
    save: bool = 0,
    title: str = "",
) -> bool:
    """
    Display cell fate rates 

    Parameters
    ----------
    fate_cell : np.ndarray 
        cell fate rates in this order [div_sc, 
                                       diff_sc_pc, 
                                       div_pc, 
                                       diff_pc_gc, 
                                       diff_pc_ent,
                                       ext]
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    label_cell_fate: list
        list of string used for legend
    colors : np.ndarray optionnal 
            plot colors 
    file_name : str, optional
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title : str : optionnal 
        Plot title 
    
    """
    if colors.shape[0] < 1:
        import matplotlib.colors as mcolors

        clrs = list(mcolors.TABLEAU_COLORS.values())
    else:
        clrs = colors

    for i in range(len(fate_cell) - 1):

        plt.plot(
            z_coord, fate_cell[i], label=label_cell_fate[i], lw=3, color=clrs[i],
        )
    plt.plot(
        z_coord, fate_cell[-1], label="Ext", lw=3, color=clrs[-1],
    )
    plt.xlabel("$\mu m$")
    plt.legend(loc="upper right")
    plt.ylabel("Rate $h^{-1}$")
    plt.legend()
    plt.xlim(0, 200)
    plt.legend(loc="upper right")
    plt.title(title)
    plt.grid("major", color="gray", linestyle="dashed", linewidth=1)
    if save:
        import tikzplotlib

        tikzplotlib.clean_figure()
        tikzplotlib.save("TEX/" + file_name + ".tex")
    plt.show()
    plt.clf()
    return 1


def show_dcs_shape(
    z_coord: np.ndarray,
    dict_dcs_shape: dict,
    nb_dcs: float,
    file_name: str = "dcs_shape",
    save: bool = 0,
    title: str = "",
):
    """
    Display densities of cell beiing part of cell fate

    Parameters
    ----------
    z_coord : np.ndarray
        Scapce coordinates along the crypte
    dict_dcs_shape : dict
        different shape stored in a dictionnary values are lists of 4 floats
        in order z_a, a, z_b, b used with the function dcs_shape
    nb_dcs : float
        Total density of dcs cell 
    file_name : str, optional
        file name in which user can stor graphe results in tex format.
    save : TYPE, optional
        choice for user to save graphe. The default is 0.
    title : str : optionnal 
        Plot title 
    
    """

    for shape, val in dict_dcs_shape.items():
        if len(val) != 4:
            raise ValueError('"wrong list of values for ', shape)

        dcs = np.array([dcs_shape(z, val[0], val[1], val[2], val[3]) for z in z_coord])
        rho_dcs = (nb_dcs / simps(dcs, z_coord)) * dcs
        plt.plot(
            z_coord,
            rho_dcs,
            label=" $z_a = "
            + str(val[0])
            + ",  a = "
            + str(round(val[1], 2))
            + ",  z_b = "
            + str(val[2])
            + ",  b = "
            + str(round(val[3], 2))
            + "$",
        )

    plt.legend(loc="upper right")
    plt.title("Different $dcs$ cell densities")
    plt.xlabel("$\mu m$")
    plt.ylabel("density")
    plt.xlim(0, 200)
    plt.title(title)
    if save:
        import tikzplotlib

        tikzplotlib.clean_figure()
        tikzplotlib.save("TEX/" + file_name + ".tex")
    plt.show()
    plt.clf()
    return 1
