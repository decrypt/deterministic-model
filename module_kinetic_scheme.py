#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 
"Ref Aregba Driollet 2011 Diffusiv Kinetic explicit scheme for parabolic equations"

"""


import numpy as np
from numba import jit


@jit(nopython=True)
def Get_B_ronde(B: np.ndarray, m: float) -> np.ndarray:
    """
    Get $\mathbb{B}$ from B and m using equation (3.29) 

    Parameters
    ----------
    B : numpy
        Terme de diffusion
    m : float in [0, 1/4]

    Returns
    -------
    B_ronde : numpy array

    """
    B_ronde = np.zeros(B.shape[0])
    B_ronde[1:-1] = m * B[:-2] + (1 - 2 * m) * B[1:-1] + m * B[2:]
    B_ronde[0] = (1 - 2 * m) * B[0] + m * B[1]
    B_ronde[-1] = m * B[-2] + (1 - 2 * m) * B[-1]
    return B_ronde


@jit(nopython=True)
def Get_theta(B_prime: np.ndarray, alpha: float) -> float:
    """
    Get theta according to equation (2.6)

    Parameters
    ----------
    B_prime : np.ndarray
        Derivative of B according to the state variable u.
    alpha : float \în ]0,1[

    Returns
    -------
    float
        theta.

    """
    return np.sqrt(max(B_prime) / (1 - alpha)) + 0.000001


@jit(nopython=True)
def Get_lambda(A_prime: np.ndarray) -> float:
    """
    Get lamdba sush as eq (2.4) holds true

    Parameters
    ----------
    A_prime : np.ndarray
        Derivative of B according to the state variable u.

    Returns
    -------
    float
        lambda

    """
    return max(np.abs(A_prime)) + 0.000001


@jit(nopython=True)
def Maxwellian_d(
    u: np.ndarray,
    A: np.ndarray,
    B: np.ndarray,
    lambda_p: float,
    lambda_m: float,
    theta: float,
    D: int = 1,
) -> np.ndarray:
    """
    Return Maxwellian function from equation (2.3) when d <= D 

    Parameters
    ----------
    u : np.ndarray
        state variable
    A : np.ndarray
        correspond to the transport term see (1.1)
    B : np.ndarray
        corrspond to the iffusion term, may be nonlinear see (1.1).
    lambda_p : float
        velocity - Assuring monotony parameter 
    lambda_m : float < lambda_p 
        velocity 
    theta : float > 0 
        Assuring monotony parameter 
    D : int
        Dimension.

    """
    return (lambda_p / D * (u - B / theta ** 2) - A) / (lambda_p - lambda_m)


@jit(nopython=True)
def Maxwellian_D_plus_d(
    u: np.ndarray,
    A: np.ndarray,
    B: np.ndarray,
    lambda_p: float,
    lambda_m: float,
    theta: float,
    D: int = 1,
) -> np.ndarray:
    """
    Return Maxwellian function from equation (2.3) when  D < d <= 2D 

    Parameters
    ----------
    u : np.ndarray
        state variable
    A : np.ndarray
        correspond to the transport term see (1.1)
    B : np.ndarray
        corrspond to the iffusion term, may be nonlinear see (1.1).
    lambda_p : float
        velocity - Assuring monotony parameter 
    lambda_m : float < lambda_p 
        velocity 
    theta : float > 0 
        Assuring monotony parameter 
    D : int
        Dimension.

    """
    return (-lambda_m / D * (u - B / theta ** 2) + A) / (lambda_p - lambda_m)


@jit(nopython=True)
def minmod(r: float) -> float:
    """
    Return minmod limitor

    Parameters
    ----------
    r : float
        Ratio of succesiv gradients
    """
    return max(0, min(1, r))


@jit(nopython=True)
def van_leer(r: float) -> float:
    """
    Return Van-Leer limitor

    Parameters
    ----------
    r : float
        Ratio of succesiv gradients
    """
    return (r + np.abs(r)) / (1 + np.abs(r))


@jit(nopython=True)
def superbee(r: float) -> float:
    """
    Return Superbee limitor

    Parameters
    ----------
    r : float
        Ratio of succesiv gradients
    """
    return max(0.0, min(1, 2 * r), min(2, r))


@jit(nopython=True)
def Lax_Wendroff_limiters(
    u: np.ndarray, a: float, dt: float, dz: float, choice=superbee
) -> np.ndarray:
    """
    Return flux obtained with the Lax-Wendroff version of flux limitor

    Parameters
    ----------
    u : np.ndarray
        state variable.
    a : float
        speed
    dt : float
        time step 
    dz : float
        space step
    choice : function, optional
        DESCRIPTION. The default is superbee. Can be van_leer or minmod 

    Returns
    -------
    np.ndarray : the flux 
      
    """

    limit = np.zeros(u.shape[0] - 1)

    for i in range(1, limit.shape[0]):
        if (u[i] - u[i - 1]) * (u[i + 1] - u[i]) > 0:
            r = (u[i] - u[i - 1]) / (u[i + 1] - u[i])
            limit[i] = choice(r)
    c = a * dt / dz
    return a * u[:-1] + limit * a * (1 - c) / 2 * (u[1:] - u[:-1])


@jit(nopython=True)
def Get_A_B_conserv_and_dt(
    u: np.ndarray,
    A: np.ndarray,
    A_prime: np.ndarray,
    B: np.ndarray,
    B_prime: np.ndarray,
    alpha: float,
    dt: float,
    dz: float,
    m: float,
) -> (np.ndarray, np.ndarray, float):
    """
    Get $\mathbb{A}$, $\mathbb{B}$, and  the new time step accordind to 
    proposition 3.1, equation 3.27 and 3.32 IN dimension 1 
    be carefull : dt is crushed
    In dimension 1 we consider lambda_m = -lambda_p 
    
    Parameters
    ----------
    u : np.ndarray
        state variable
    A : np.ndarray
        correspond to the transport term see (1.1)
    A_prime : np.ndarray
        DESCRIPTION.
    B : np.ndarray
        corrspond to the iffusion term, may be nonlinear see (1.1).
    B_prime : np.ndarray
        DESCRIPTION.
    alpha : float \in ]0,1[
        Used to get theta
    dt : float
        time step
    dz : float
        space step 
    m : float in [0,1/4]
        Used to get $\mathbb{B}$
    D : int optinal 
        Dimension, the defoult value is 1 

    """
    # BGK model parameters such as Maxwellian functions are monotone
    lamb = Get_lambda(A_prime)
    theta = Get_theta(B_prime, alpha)
    # Maxwellien functions compatible with equation (1.1)
    M1 = Maxwellian_d(u, A, B, lamb, -lamb, theta)
    M2 = Maxwellian_D_plus_d(u, A, B, lamb, -lamb, theta)

    # Flux for the N = 2 first equation of the BGK model are approximated with
    # a MUSCL type scheme actually we use the Lax-Wendroff limitor scheme
    F1 = Lax_Wendroff_limiters(M1, lamb, dt, dz)
    F2 = Lax_Wendroff_limiters(M2, -lamb, dt, dz)

    # Get A_ronde ( 3.27)
    A_ronde = lamb * F1 - lamb * F2

    # Calcul B_ronde proposition 3.1
    B_ronde = Get_B_ronde(B, m)

    # get new dt  (3.32)
    dt = min(dz ** 2 / (2 * theta ** 2 * (1 - 3 * m)), dz / lamb)

    return A_ronde, B_ronde, dt
