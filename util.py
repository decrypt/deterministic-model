#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 16:32:45 2021

@author: mhaghebaert
"""

""" colors definition """
import numpy as np
import pandas as pd

"Used to generate SA result graphe 12 colors"
FALU_RED = np.array([134, 29, 33])
RUBY_RED = np.array([155, 34, 38])
RUFOUS = np.array([174, 32, 18])
MAHOGANY = np.array([187, 62, 3])
ALLOY_ORANGE = np.array([202, 103, 2])
GAMBOGE = np.array([238, 155, 0])
MEDIUM_CHAMPAGNE = np.array([233, 216, 189])
MIDDLE_BLUE_GREEN = np.array([148, 210, 189])
VIRIDIAN_GREEN = np.array([10, 147, 150])
BLUE_SAPPHIRE = np.array([0, 95, 117])
PRUSSIAN_BLUE = np.array([0, 44, 61])
RICH_BLACK = np.array([0, 29, 41])

color_SA_12_RGB = np.array(
    [
        FALU_RED,
        RUBY_RED,
        RUFOUS,
        MAHOGANY,
        ALLOY_ORANGE,
        GAMBOGE,
        MEDIUM_CHAMPAGNE,
        MIDDLE_BLUE_GREEN,
        VIRIDIAN_GREEN,
        BLUE_SAPPHIRE,
        PRUSSIAN_BLUE,
        RICH_BLACK,
    ]
)
color_SA_12_rgb = color_SA_12_RGB / 256

" colours to represent index convergence"

color_indices = (213, 122, 102)
color_screening = (244, 185, 66)
color_ranking = (57, 47, 90)
color_threshold = (88, 123, 127)

conv = np.array([color_indices, color_screening, color_ranking, color_threshold]) / 256

""" cell fate colors """

div_sc = np.array([38, 70, 83])
dif_sc = np.array([231, 29, 54]) 
div_pc = np.array([255, 159, 28])
dif_pc_mc = np.array([46, 196, 182])
dif_pc_ent = np.array([0, 126, 167])
ext_mc = np.array([194, 168, 62])
ext_ent = np.array([76, 35, 10])

colors_cell_fate_RGB = np.array(
    [div_sc, ext_mc, div_pc, dif_sc, dif_pc_mc, dif_pc_ent, ext_ent]
)
colors_cell_fate_rgb = colors_cell_fate_RGB / 256


""" Densities """
tot_dens = [0, 0, 0]
tot_sc = [100, 149, 237]
tot_dcs = [128, 128, 128]
tot_pc = [214, 39, 40]
tot_gc = [106, 90, 205]
tot_ent = [44, 160, 44]

colors_density_leo_RGB = np.array([tot_sc, tot_dcs, tot_pc, tot_gc, tot_ent])
colors_density_leo_rgb = colors_density_leo_RGB / 256


""" schema notebook"""
user = [255, 107, 53]
notebook = [247, 197, 159]
file = [239, 239, 208]
lib = [0, 78, 137]
py_file = [26, 101, 158]
colors_scheme_code = np.array([user, notebook, file, lib, py_file])

#%%
""" Functions """


def img_tikz_to_png(file_save: str) -> int:
    """
    Create the png format of a tikzplotlib graph 

    Parameters
    ----------
    file_save : str
        file containing results from tikzplotlib plots. Attention the file must 
        be in the TEX container . SUREMENT A MODIFIER  ou faire un script avec la dernière partie ? 

    """
    import subprocess, os

    preamble = "\\documentclass[convert]{standalone}\n\\usepackage[utf8]{inputenc}\n\\usepackage{pgfplots}\n\\DeclareUnicodeCharacter{2212}{−}\n\\usepgfplotslibrary{groupplots,dateplot}\n\\usetikzlibrary{patterns,shapes.arrows}\n\\pgfplotsset{compat=newest}\n\\begin{document}\n"
    end = "\\end{document}"
    with open(file_save, "r") as fic:
        lignes = fic.read()
        test = lignes[:38]

    if test == str("% This file was created by tikzplotlib"):
        print(preamble + str(lignes) + end)
        with open(file_save, "w") as fic:
            fic.write(preamble + lignes + end)

    subprocess.call(["pdflatex", file_save])
    subprocess.call(["pdftoppm", file_save[4:-4] + ".pdf", file_save[:-4], "-png"])
    subprocess.call(
        [
            "rm",
            file_save[4:-4] + ".pdf",
            file_save[4:-4] + ".aux",
            file_save[4:-4] + "log",
        ]
    )
    return 0


# img_tikz_to_png('TEX/JOMB_cell_fate.tex')
# img_tikz_to_png("TEX/sigma_on_mu_star_Y_density.tex")
# img_tikz_to_png("TEX/convergence_index_Y_density.tex")
# img_tikz_to_png("TEX/convergence_index_Y_div_rate_pc.tex")
# img_tikz_to_png("TEX/convergence_index_Y_ratio_O_bot_top.tex")


def dict_to_df(dico: dict):
    """Convert dict structure into Pandas DataFrame."""
    return pd.DataFrame({k: v for k, v in dico.items()}, index=None)


def write_fic(fic_name: str, data: pd.DataFrame) -> int:
    """
    Write results in a file in csv format using dataframes

    Parameters
    ----------
    fic_name : string
        Backup file name
    data : dict
        Data to save
    """

    frame = data.to_df()
    frame.to_csv(fic_name, sep=str("\t"))

    return 1


def read_fic(fic_name: str) -> int:
    """
    Read data from a file in binary format

    Parameters
    ----------
    fic_name : str
        file to load
    Returns
    -------
    data : all type
    """
    return pd.read_csv(fic_name, delimiter="\t", index_col=0)


#%% Latex names

name_latex = [
    "$W$",
    "$q_{div,sc}^\infty$",
    "$K_{div,sc}[z]$",
    "$l_{div,sc}[z]$",
    "$K_{div,sc}[dens]$",
    "$l_{div,sc}[dens]$",
    "$K_{div,sc}[but]$",
    "$l_{div,sc}[but]$",
    "$q_{sc,pc}^\infty$",
    "$q_{div,pc}^\infty$",
    "$K_{div,pc}[z]$",
    "$l_{div,pc}[z]$",
    "$K_{div,pc}[dens]$",
    "$q_{pc,ent}^\infty$",
    "$l_{pc,ent}[z]$",
    "$K_{pc,ent}[but]$",
    "$q_{ext,gc}^\infty$",
    "$K_{ext,gc}[z]$",
    "$l_{ext,gc}[z]$",
    "$K_{ext,gc}[dens]$",
    "$\sigma_{o}$",
    "$\gamma_{\\beta}^\infty$",
    "$K_{\\beta}$",
    "$c_{b,lum}$",
    "$c_{o,bot}$",
    "$\\rho_{sc}^{bot}$",
]


output_name_latex = [
    "$\\rho$",
    "$\\rho_{sc}$",
    "$\\rho_{sc}$",
    "$\\rho_{sc}$",
    "$\\rho_{sc}$",
    "$\\bar{z}_{sc}$",
    "$\\bar{z}_{pc}$",
    "$\\bar{z}_{gc}$",
    "$\\bar{z}_{ent}$",
    "$c_o^{top}$",
    "$c_b^{bot}$",
    "$O_2  bot/top$",
    "$rate_{div,sc}$",
    "$rate_{div,pc}$",
    "$rate_{dif,sc,pc}$",
    "$rate_{dif,pc,gc}$",
    "$rate_{div,pc,ent}$",
    "$rate_{ext,gc}$",
    "$rate_{ext,ent}$",
    "$\\rho_{pc} / \\rho$",
]

label_cell_fate_latex = [
    "Div sc ",
    "Diff sc / pc ",
    "Div pc",
    "Diff pc / gc ",
    "Diff pc / ent ",
    "Ext gc ",
    "Ext ent ",
]
