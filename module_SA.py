#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 14:28:18 2020

@author: mhaghebaert

READ ME : 
# See SALib instructions for more info : https://salib.readthedocs.io/en/latest/api.html#method-of-morris
# See SAFE Toolbok instruction for more info : https://www.safetoolbox.info/
The following code is organised in different parts: 
    - Sensitivity analysis settings and outputs generation : parralelisation 
    and use of SALib toolboox  (SA_generate_Y_from_X, resolution_parallel, 
                                dict_SA_form, medium_position)
    - Sensitivity Indicies and convergence use of SAFE Toolbox 
        (use_SAFE_indices_and_convergence, 
         rank_factor_boots, get_rank_index, screening_factor)
    - Display results (plot_sigma_mu_star, display_ind_convergence)
"""

# Libraries
import numpy as np
from scipy.integrate import simps
import pickle
from multiprocessing import Pool, cpu_count
from functools import partial
import scipy.stats as ss
import matplotlib.pyplot as plt
from numpy.matlib import repmat
import seaborn as sns

# SA libraries
from SALib.sample import morris


# Modules
import module_model_resolution as reso


def SA_generate_Y_from_X(
    saving_file: str,
    nb_sample: int,
    dico_param_SA: dict,
    y0: np.ndarray,
    dico_param: dict,
    dico_param_num: dict,
    dico_space: dict,
    name_latex: list = [],
    nb_optimal_traj: int = 0,
    nb_CPU: int = 1,
) -> dict:

    """
    Function used to generate Y (outputs) from X (input) to perform Morris 
    sensitivity analysis. It uses parral calcul 
    Parameters
    ----------
    saving_file: str,
        name of the file to save results
    nb_sample: int,
        number of trajectories to generate
    dico_param_SA: dict,
        Dict containning parameter's names studied in SA, their initial values and the percent of variation
    y0: np.ndarray,
        Initial condition for cell densities and metabolite concentrations
    dico_param: dict,
        Dict containing all parameters used to solve the model 
    dico_param_num: dict,
        Dict containing numeric parameters 
    dico_space: dict,
        Dict containing all table of space variable that are constant in time
        in the model 
    nb_optimal_traj: int = 0,
        option proposed by SALib to use the methode describe in Campolongo 2007 to optimize trajectorie generation
    nb_processes: int = 1,
        Number of processor to use in the parallelisation calculs
    """
    if nb_CPU > cpu_count():
        raise ValueError("STOP choose a number of CPU <=  ", cpu_count())
    # Create problem see SALib instruction
    problem = dict_SA_form(dico_param_SA)

    # calculation of trajectories using SALib options for otimal trajectories generation
    if nb_optimal_traj != 0:
        X = morris.sample(
            problem, N=nb_optimal_traj, num_levels=4, optimal_trajectories=nb_sample
        )
    else:
        X = morris.sample(problem, N=nb_sample, num_levels=4)

    ## Get all Y corresponding to each point of X
    pool = Pool(processes=nb_CPU)
    func = partial(
        resolution_parallel, y0, dico_param, dico_param_num, dico_space, dico_param_SA,
    )
    res = pool.map_async(func, X)
    pool.close()
    pool.join()
    res_paral = res.get()

    # Recovering the results of the parallelisation
    X = [res_paral[0][0]]
    Y = [res_paral[0][1]]
    TAUX = [res_paral[0][2]]
    TF = res_paral[0][3]
    for i in range(1, len(res_paral)):
        X = np.append(X, [res_paral[i][0]], axis=0)
        Y = np.append(Y, [res_paral[i][1]], axis=0)
        TAUX = np.append(TAUX, [res_paral[i][2]], axis=0)
        TF = np.append(TF, [res_paral[i][3]])

    RHO = Y[:, 0] + Y[:, 1] + Y[:, 2] + Y[:, 3] + Y[:, 4]

    output_names = [
        "Y_density",
        "Y_sc_density",
        "Y_pc_density",
        "Y_gc_density",
        "Y_ent_density",
        "Y_sc_medium_pos",
        "Y_pc_medium_pos",
        "Y_gc_medium_pos",
        "Y_ent_medium_pos",
        "Y_Z_top_o",
        "Y_Z_bot_b",
        "Y_ratio_O_bot_top",
        "Y_div_rate_sc",
        "Y_div_rate_pc",
        "Y_dif_rate_sc_pc",
        "Y_dif_rate_pc_gc",
        "Y_dif_rate_pc_ent",
        "Y_ex_rate_gc",
        "Y_ex_rate_ent",
        "Y_ratio_pc_tot",
    ]

    z_coord = dico_space["z_coord"]
    dict_outputs = {
        "problem": problem,
        "R": nb_sample,
        "traj_opt": nb_optimal_traj,
        "design_type": "trajectory",
        "X": X,
        "output_names": output_names,
        "name_latex": name_latex,
        "Y_density": simps(RHO, z_coord),
        "Y_sc_density": simps(Y[:, 0], z_coord),
        "Y_pc_density": simps(Y[:, 2], z_coord),
        "Y_gc_density": simps(Y[:, 3], z_coord),
        "Y_ent_density": simps(Y[:, 4], z_coord),
        "Y_sc_medium_pos": np.array(
            [medium_position(Y[:, 0][i], z_coord) for i in range(len(Y))]
        ),
        "Y_pc_medium_pos": np.array(
            [medium_position(Y[:, 2][i], z_coord) for i in range(len(Y))]
        ),
        "Y_gc_medium_pos": np.array(
            [medium_position(Y[:, 3][i], z_coord) for i in range(len(Y))]
        ),
        "Y_ent_medium_pos": np.array(
            [medium_position(Y[:, 4][i], z_coord) for i in range(len(Y))]
        ),
        "Y_Z_top_o": np.array([Y[:, 5][i][-2] for i in range(len(Y))]),
        "Y_Z_bot_b": np.array([Y[:, 6][i][1] for i in range(len(Y))]),
        "Y_ratio_O_bot_top": np.array(
            [Y[:, 5][i][1] / Y[:, 5][i][-2] for i in range(len(Y))]
        ),
        "Y_div_rate_sc": simps(TAUX[:, 0] * Y[:, 0], z_coord) / simps(Y[:, 0], z_coord),
        "Y_dif_rate_sc_pc": simps(TAUX[:, 1] * Y[:, 0], z_coord)
        / simps(Y[:, 0], z_coord),
        "Y_div_rate_pc": simps(TAUX[:, 2] * Y[:, 2], z_coord) / simps(Y[:, 2], z_coord),
        "Y_dif_rate_pc_gc": simps(TAUX[:, 3] * Y[:, 2], z_coord)
        / simps(Y[:, 2], z_coord),
        "Y_dif_rate_pc_ent": simps(TAUX[:, 4] * Y[:, 2], z_coord)
        / simps(Y[:, 2], z_coord),
        "Y_ex_rate_gc": simps(TAUX[:, 5] * Y[:, 3], z_coord) / simps(Y[:, 3], z_coord),
        "Y_ex_rate_ent": simps(TAUX[:, 5] * Y[:, 4], z_coord) / simps(Y[:, 4], z_coord),
        "Y_ratio_pc_tot": simps(Y[:, 2], z_coord) / simps(RHO, z_coord),
        "TF": TF,
    }

    with open(saving_file, "wb") as file:
        pickle.dump(dict_outputs, file)

    return dict_outputs


def resolution_parallel(
    y0: np.ndarray,
    dico_param: dict,
    dico_param_num: dict,
    dico_space: dict,
    dico_param_SA: dict,
    X: np.ndarray,
) -> (np.ndarray, np.ndarray, np.ndarray, float):
    """
    function used to perform SA in parallel. 
    
    Parameters
    ----------
    y0: np.ndarray,
        Initial condition for cell densities and metabolite concentrations
    dico_param: dict,
        Dict containing all parameters used to solve the model 
    dico_param_num: dict,
        Dict containing numeric parameters 
    dico_space: dict,
        Dict containing all table of space variable that are constant in time
        in the model 
    dico_param_SA: dict,
        Dict containning parameter's names studied in SA
    X: np.ndarray,
        point in the parameters space of SA. table containing values for 
        parameters listed in dico_param_SA
    
    """

    # Update of parameters
    i = 0
    for cle in dico_param_SA.keys():
        dico_param[cle] = X[i]
        i += 1

    # update of space varaible according to the new parameters
    dico_space["R_sc"] = reso.regulation_r(
        dico_space["z_coord"], dico_param["K_div_sc"], dico_param["l_div_sc"]
    )
    dico_space["R_pc"] = reso.regulation_r(
        dico_space["z_coord"], dico_param["K_div_pc"], dico_param["l_div_pc"]
    )
    dico_space["R_pc_gc_ent"] = reso.regulation_r(
        dico_space["z_coord"], dico_param["K_div_pc"], dico_param["l_pc_ent"]
    )
    dico_space["R_ext"] = reso.regulation_r(
        dico_space["z_coord"], dico_param["K_ext"], dico_param["l_ext"]
    )
    dico_space["G"] = reso.Get_G(
        dico_space["C"], dico_space["phi"], dico_param["W_barre"]
    )
    dico_space["G_prime"] = reso.Get_G_prime(
        dico_space["C"],
        dico_space["C_prime"],
        dico_space["phi"],
        dico_space["phi_prime"],
        dico_param["W_barre"],
    )
    dico_space["G_second"] = reso.Get_G_second(
        dico_space["C"],
        dico_space["C_prime"],
        dico_space["C_second"],
        dico_space["phi"],
        dico_space["phi_prime"],
        dico_space["phi_second"],
        dico_param["W_barre"],
    )

    dico_param["q_pc_gc"] = dico_param["q_pc_ent"] / 3
    dico_param["sigma_b"] = dico_param["sigma_o"]
    # Model resolution
    y, n_iter, rho_cell_fate, tf = reso.resolution(
        y0, dico_param, dico_param_num, dico_space
    )

    return X, y, rho_cell_fate, tf


def dict_SA_form(dict_SA: dict) -> dict:
    """
    Create a dictionnary used with SALib library to perform SA. 

    Parameters
    ----------
    dict_SA : dict
        Dict containning studied parameters in SA and their corresponding 
        percentage of variation.
    """

    num_var = len(dict_SA)
    names = list(dict_SA.keys())
    # Create boundary values from initial value and a percent of variation
    boundaries = np.zeros([num_var, 2])
    i = 0
    for param, value in dict_SA.items():
        percent = value[1]

        if len(value) != 2:
            raise ValueError("Wrong value definition for : ", param)
        if percent > 100:
            raise ValueError("Percent shoulb be <100 for param : ", param)
        if percent < 0:
            raise ValueError("Percent shoulb be positif for param : ", param)

        if value[0] >= 0:
            boundaries[i][0] = value[0] - percent * value[0] / 100
            boundaries[i][1] = value[0] + percent * value[0] / 100
        elif value < 0:
            boundaries[i][0] = value[0] + percent * value[0] / 100
            boundaries[i][1] = value[0] - percent * value[0] / 100
        i += 1
    dico = {"num_vars": num_var, "names": names, "bounds": boundaries}
    return dico


def medium_position(rho: np.ndarray, z: np.ndarray) -> np.ndarray:
    """
    Return medium position in the crypt of cells for the density rho 

    Parameters
    ----------
    rho : np.ndarray
        Density
    z : np.ndarray
        space coordinates.

    """
    return simps(rho * z, z) / simps(rho, z)


def use_SAFE_indices_and_convergence(
    R: int,
    xmin: np.ndarray,
    xmax: np.ndarray,
    X: np.ndarray,
    Y: np.ndarray,
    design_type: str = "trajectory",
    Nboot: int = 1000,
    screening_threshold: float = 0.05,
):
    import SAFEpython.EET as EET  # module to perform the EET
    from SAFEpython.util import aggregate_boot  # aggregate the bootstrap results
    # Compute EET Morris indices
    mu, mu_star, sigma, _ = EET.EET_indices(R, xmin, xmax, X, Y, design_type)

    # compute Ranking, Screening and Index indices convergence.
    # Use bootstrapping to derive confidence bounds:
    Nboot = 1000
    # Compute sensitivity indices for Nboot bootstrap resamples:
    mu_boot, mu_star_boot, sigma_boot, EE_boot = EET.EET_indices(
        R, xmin, xmax, X, Y, design_type, Nboot=Nboot,
    )

    # Repeat computations using a decreasing number of samples so as to assess
    # if convergence was reached within the available dataset:
    if R < 5:
        rr = np.linspace(1, R, R).astype(int)
    else:
        rr = np.linspace(5, R, min(10, R)).astype(int)

    mu_conv, mu_star_conv, sigma_conv = EET.EET_convergence(EE_boot, rr, Nboot)

    # Normalise

    mu_star_conv = [
        mu_star_conv[i] / np.max(mu_star_conv[i]) for i in range(len(mu_star_conv))
    ]

    mi_star_agreg, mi_star_agreg_lb, mi_star_agreg_ub = aggregate_boot(
        mu_star_conv
    )  # shape (R,M)

    # Ranking factor
    stat_ranking = np.array(
        [rank_factor_boots(np.array(mu_star_conv[i])) for i in range(rr.shape[0])]
    )

    # Indices factor
    stat_ind = np.array(
        [
            max(mi_star_agreg_ub[i] - mi_star_agreg_lb[i])
            for i in range(mi_star_agreg_ub.shape[0])
        ]
    )

    # Screening factor
    stat_screen = screening_factor(
        mi_star_agreg, mi_star_agreg_lb, mi_star_agreg_ub, screening_threshold
    )

    dict_SA_indices = {
        "mu": mu,
        "mu_star": mu_star,
        "sigma": sigma,
    }
    dict_SA_convergence = {
        "rr": rr,
        "stat_ranking": stat_ranking,
        "stat_ind": stat_ind,
        "stat_screening": stat_screen,
    }

    return dict_SA_indices, dict_SA_convergence


def rank_factor_boots(mi_rr: np.ndarray) -> float:
    Nboot = len(mi_rr)
    nb_param = len(mi_rr[0])

    rank_index = []
    # rank data create an array of int Nboot * nb_param
    rank_boot = np.zeros_like(mi_rr)

    # get rank index for every paires of bootstrap resampling
    for i in range(Nboot):
        # get all rank
        rank_boot[i] = nb_param - ss.rankdata(mi_rr[i]).astype(int) + 1
        for j in range(i):
            rank_index += [
                get_rank_index(mi_rr[i], rank_boot[i], mi_rr[j], rank_boot[j])
            ]
    return np.percentile(rank_index, 95)


def get_rank_index(
    indice_val_1: np.ndarray,
    rank_1: np.ndarray,
    indice_val_2: np.ndarray,
    rank_2: np.ndarray,
) -> float:
    rank = np.abs(rank_1 - rank_2)
    max_up = np.maximum(indice_val_1, indice_val_2) ** 2
    max_down = np.sum(max_up)
    rho_s = np.sum(rank * max_up / max_down)
    return rho_s


def screening_factor(
    mi: np.ndarray, mi_lb: np.ndarray, mi_ub: np.ndarray, threshold: float
) -> np.ndarray:
    insensitive_sub = np.where(
        np.array(mi) < threshold, np.array(mi), np.zeros_like(np.array(mi)),
    )
    insensitive_mi_lb = np.where(insensitive_sub == 0, mi_lb * 0, mi_lb)
    insensitive_mi_ub = np.where(insensitive_sub == 0, mi_ub * 0, mi_ub)
    return np.array(
        [
            max(insensitive_mi_ub[i] - insensitive_mi_lb[i])
            for i in range(insensitive_mi_lb.shape[0])
        ]
    )


def plot_sigma_mu_star(
    mi: np.ndarray,
    mi_star: np.ndarray,
    sigma: np.ndarray,
    labelinput: list = [],
    colors: np.ndarray = np.array([]),
    SAVE: bool = False,
    saving_file: str = "sigma_on_mu",
):

    """ Plot the sensitivity indices computed by the Elementary Effects Test -
    mean (mi) of the EEs on the horizontal axis and standard deviation (sigma)
    of the EEs on the vertical axis.
    
   

    Inputs:
            mi = mean of the elementary effects             - numpy.ndarray(M,)
         sigma = standard deviation of the elementary       - numpy.ndarray(M,)
                 effects

    This function is this function is inspired by the EET.plot function of 
    the SAFE Toolbox created by  F. Pianosi, F. Sarrazin
    and T. Wagener at Bristol University (2015).
    """

    # Options for the graphic
    ms = 10  # Marker size
    nb_legend = 12  # number of input names that will be displayed in the legend

    ###########################################################################
    # Check inputs
    ###########################################################################
    if mi.dtype.kind != "f" and mi.dtype.kind != "i" and mi.dtype.kind != "u":
        raise ValueError('"mi" must contain floats or integers.')
    if (
        mi_star.dtype.kind != "f"
        and mi_star.dtype.kind != "i"
        and mi_star.dtype.kind != "u"
    ):
        raise ValueError('"mi_star" must contain floats or integers.')
    if sigma.dtype.kind != "f" and sigma.dtype.kind != "i" and sigma.dtype.kind != "u":
        raise ValueError('"sigma" must contain floats or integers.')

    Nm = mi.shape
    if len(Nm) > 1:
        raise ValueError('"mi" must be of size (M, ).')
    M = Nm[0]
    Nm_star = mi_star.shape
    if len(Nm_star) > 1:
        raise ValueError('"mi_star" must be of size (M, ).')
    Ns = sigma.shape
    if len(Ns) > 1:
        raise ValueError('"sigma" must be of size (M, ).')
    if Ns[0] != M:
        raise ValueError('"mi" and "sigma" must have the same number of elements')
    if Ns[0] != Nm_star[0]:
        raise ValueError('"mi_star" and "sigma" must have the same number of elements')
    if Nm_star[0] != M:
        raise ValueError('"mi" and "mi_star" must have the same number of elements')

    ###########################################################################
    # Check optional inputs
    ###########################################################################
    if not labelinput:
        labelinput = [np.nan] * M
        for i in range(M):
            labelinput[i] = "X" + str(i + 1)
    else:
        if not all(isinstance(i, str) for i in labelinput):
            raise ValueError('Elements in "labelinput" must be strings.')
        if len(labelinput) != M:
            raise ValueError('"labelinput" must have M elements.')

    if colors.shape[0] < 1:
        col = (
            np.array(
                [
                    [228, 26, 28],
                    [55, 126, 184],
                    [77, 175, 74],
                    [152, 78, 163],
                    [255, 127, 0],
                ]
            )
            / 256
        )
        A = len(col)
        L = int(np.ceil(M / A))
        clrs = repmat(col, L, 1)
    else:
        clrs = colors

    ###########################################################################
    # Produce plot
    ###########################################################################

    labelinput_new = [np.nan] * M

    # Sorting index
    Sidx = np.flip(np.argsort(mi_star), axis=0)
    mi_star = mi_star[Sidx]
    mi = mi[Sidx]
    sigma = sigma[Sidx]
    for i in range(M):
        labelinput_new[i] = labelinput[Sidx[i]]

    if nb_legend < M:
        labelinput_new = labelinput_new[0 : nb_legend + 1]
        labelinput_new[-1] = "less influent"
    if nb_legend > M:
        nb_legend = M

    plt.figure()

    # First plot mu* as square or circle according to mu sign:
    for i in range(nb_legend):
        if mi[i] >= 0:
            plt.plot(
                mi_star[i],
                sigma[i],
                "ok",
                markerfacecolor=clrs[i],
                markersize=ms,
                markeredgecolor=clrs[i],
            )
        else:
            plt.plot(
                mi_star[i],
                sigma[i],
                "sk",
                markerfacecolor=clrs[i],
                markersize=ms,
                markeredgecolor=clrs[i],
            )
    # Second plot less influent parameters
    for i in range(M - nb_legend, M):
        plt.plot(
            mi_star[i],
            sigma[i],
            "ok",
            markerfacecolor="black",
            markersize=ms / 2,
            markeredgecolor="k",
        )

    plt.legend(
        labelinput_new, bbox_to_anchor=(1.1, -0.1), loc="lower left", ncol=1, fontsize=9
    )
    plt.xlabel("$\\sigma$")
    plt.ylabel("$\\mu^*$")
    plt.grid(linestyle="--")
    plt.xlim(0)
    if SAVE:
        import tikzplotlib

        tikzplotlib.save("TEX/" + saving_file + ".tex")
    plt.show()

    return 1


def display_ind_convergence(
    rr,
    stat_screening,
    stat_ranking,
    col_perso,
    threshold=0.05,
    SAVE=False,
    saving_file="convergence_index",
):
    plt.rcParams.update(
        {"text.usetex": False,}
    )

    # plt.plot(rr, stat_ind, label="Indices", lw=3)

    plt.plot(rr, stat_screening, label="Screening", color=col_perso[1], lw=3)

    plt.plot(
        rr,
        stat_ranking * threshold,
        label="Ranking $\\times 0.05$",
        color=col_perso[2],
        lw=3,
    )

    plt.plot(
        rr,
        threshold * np.ones(rr.shape[0]),
        color=col_perso[3],
        label="Threshold = $%.2f$" % threshold,
        linestyle="dashed",
        lw=2,
    )
    plt.xlim(0)
    plt.ylim(0)
    plt.xlabel("Trajectories")
    plt.ylabel("Convergence criteria")
    plt.yticks(
        (
            0,
            threshold,
            min(stat_ranking[0] * threshold, stat_screening[0]),
            max(stat_ranking[0] * threshold, stat_screening[0]),
        ),
        (
            0,
            threshold,
            np.round(min(stat_ranking[0] * threshold, stat_screening[0]), 2),
            np.round(max(stat_ranking[0] * threshold, stat_screening[0]), 2),
        ),
    )
    plt.legend(loc="upper right", fontsize=9)

    if SAVE:
        import tikzplotlib

        tikzplotlib.save("TEX/" + saving_file + ".tex")
    plt.show()
    return 1


def heat_map(
    dict_SA: dict,
    output_name: np.ndarray = np.array([]),
    input_name: np.ndarray = np.array([]),
    save: bool = False,
    file_name: str = "heat_map",
    title: str = "",
) -> bool:
    """
    Return a Heat map representing all parameters studied in SA and all outputs.
    The heat map represent mu* indicies normalised by by the max(mu*) for every outputs
    so that mu* are in [0,1]. Its allows to visualize clearly every outputs and 
    to highligt parameters with global influence in the model under study. 

    Parameters
    ----------
    dict_SA : dict
        dictionnary containing SA results in the form proposed by the function 
        use_SAFE_indices_and_convergence
    output_name : np.ndarray, optional
        User can proposed name of outputs to display 
    input_name : np.ndarray, optional
        DUser can proposed name of inputs to display 
    save : bool, optional
        Whether to save the plot or not 
    file_name : str, optional
        file's name in which to store the plot 
    title : str, optional
        Title of the plot 

    Returns
    -------
    bool
        DESCRIPTION.

    """

    # Display options
    nb_output = len(dict_SA["output_names"])
    nb_input = dict_SA["problem"]["num_vars"]
    if not output_name:
        output_name = [np.nan] * nb_output
        for i in range(nb_output):
            output_name[i] = "Y" + str(i + 1)
    if not input_name:
        input_name = [np.nan] * nb_input
        for i in range(nb_input):
            input_name[i] = "X" + str(i + 1)

    # heat map matrix creation
    heat_map = np.zeros((nb_output, nb_input))
    i = 0
    for outputs in dict_SA["output_names"]:
        sign = np.ones(dict_SA[outputs + "_SA_indices"]["mu"].shape[0])
        sign = np.where(dict_SA[outputs + "_SA_indices"]["mu"] < 0, -1 * sign, sign)

        heat_map[i] = (
            dict_SA[outputs + "_SA_indices"]["mu_star"]
            / max(dict_SA[outputs + "_SA_indices"]["mu_star"])
            * sign
        )
        i += 1

    # Figure display
    sns.set_theme()
    sns.set(font_scale=1)
    plt.figure(figsize=(9, 7))
    ax = sns.heatmap(
        heat_map.T,
        center=0,
        cbar_kws={"ticks": [-1, 0, 1]},
        cmap="RdBu",
        xticklabels=output_name,
        yticklabels=input_name,
    )
    ax.set_xlabel("Outputs")
    ax.set_ylabel("Parameters")
    plt.title(title)
    if save:
        plt.savefig("TEX/" + file_name, bbox_inches="tight")
    plt.show()

    return 1
