# Implementation notice of the deterministic model numeric resolution proposed by L. Darrigade and al (2021)

This project propose the resolution of the deterministic model and its sensitivity analysis (SA). 
We propose different Notebooks that will generate figures presented in the article and some complements. 
More information such as equation and reminders of the article are provide in the notebooks.

Please cite Darrigade et al., 'A PDMP model of the epithelial cell turn-over in the
intestinal crypt including microbiota-derived regulations.' (2021).

## Installation

We assume that virtualenv is installed in your python environment. If it is not the case, execute the command in your shell

```sh
pip install virtualenv
```

To install the packages needed for the code execution, we will install a new python virtual environment

```sh
virtualenv -p python3 JOMB
```

Then, activate the virual environment

```sh
source JOMB/bin/activate
```
and install the required packages


```sh
pip install -r requirements.txt
```

You are now ready to open the jupyter notebooks 

```sh
jupyter notebook
```

Select in the list one of the two jupyter notebooks (either main_resolution_model.ipynb or main_sensitivity_analysis.ipynb)



## Git folder composition 

This git folder is composed of 2 Notebooks : 
- main_resolution_model
- main_sensitivity_analysis


4 python files : 
- util.py 
- module_resolution.py
- module_SA.py
- module_kinetic_scheme.py

3 folders 
- TEX
- SIMUS
- backup_SA

Installation requirements : 
- requirements.txt 

## Results backup
Backups are present in the 3 different folders which allows the user to replicate the figures in the article without running the resolution and/or SA codes.
In TEX you will find tex and png files of figures. Options are set to overwrite these backups (see Notebooks).  
backup_SA is a folder containing results oh the sensitivity analysis perform for the article, this folder dialogues with main_sensitivity_analysis.
And finaly SIMUS folder is used to dialogue with the notebook main_experiment_parameters and to store stochastic simulation results used to compare the two models. 


## Warnings
The SAFE library used in main_sensitivity_analysis notebook is open source however you need to ask for permission fist and download the library see : https://www.safetoolbox.info/. 
Whitout permission user won't be able to use the previous notebook. 

Moreover, different parallelization parts parts are present in the code (main_experiment_parameters & main_sensitivity_analysis), we advice users to be carefull when running cells specially for SA.
Indeed results presented in the article used 90 CPU during about 24 hours (!). An example is proposed when performing a simplier SA (2 parameters).   



